# Tagbook


### Generowanie datasetu
`cd dataset_gen`

**Instalacja zależności**:

Do [pdftotext](https://github.com/jalan/pdftotext) wymagany jest kompilator c++, poppler i jego headery, więcej info na stronie biblioteki. Reszta jest pythonowa i się sama ściągnie pipem:

`sudo pip install -r requirements.txt`

**Generowanie datasetu**:

`python gen_db.py <tryb> <ilość pozycji>`

