import pdftotext
import os

debug = True

def info_print(module,string):
	print("["+module+"] "+string)
	return

def debug_print(module,string):
	if debug:
		info_print(module,string)	
	return

def merge_books(books, local_books):
	for local_book in local_books:
		trigger = True
		for book in books:
			if local_book['isbn'] in book['isbn'] or local_book['olid'] == book['olid'] or local_book['lgid'] == book['lgid']:
				book['general_tags'] = list(set(book['general_tags'] + local_book['general_tags']))
				if 'additional_tags' in local_book:
					try:
						book['additional_tags'] = list(set(book['additional_tags'] + local_book['additional_tags']))
					except KeyError:
						book['additional_tags'] = local_book['additional_tags']
				trigger = False
				break
		if trigger:
			books.append(local_book)

def write_book(module,raw,book_id,text):
	path = ("out/" if raw else "out_processed/")+module+"/"
	try:
		os.makedirs(path,0o755)
	except FileExistsError:
		pass
	path += str(book_id)+".txt"
	with open(path,'w') as f:
		f.write(text)
	return path
	
class TextExtractor:
	supported = ["pdf"]
	@staticmethod
	def check_book_supported(book):
		if book['format'] in TextExtractor.supported:
			return True
		else:
			return False
	@staticmethod
	def remove_not_supported_books(books):
		for book in books:
			if TextExtractor.check_book_supported(book) == False:
				books.remove(book)
		return
	@staticmethod
	def extract_text_from_book(book):
		for i in range(0,3):
			try:
				with open(book['raw_path'], "rb") as f:
					pdf = pdftotext.PDF(f)
				return "\n\n".join(pdf)
			except pdftotext.Error:
				if i == 2:
					return None

class CSVWriter:
	@staticmethod
	def write_header(f):
		f.write("id,general_tags,additional_tags,title,author\n")
		return
	@staticmethod
	def write_book(f,book):
		tags = ""
		for a in book['general_tags']:
			tags += a+';'
		tags = tags[:-1]
		add_tags = ""
		if 'additional_tags' in book:
			for a in book['additional_tags']:
				add_tags += a+';'
			add_tags = add_tags[:-1]
		f.write(book['id']+","+tags+","+add_tags+','+book['title'].replace(',',' ')+","+book['author'].replace(',',' ')+"\n")
		return
		
