from helpers import *
import sys
sys.path.insert(0, './external')
import preprocess_nlp

clean_options = {'remove_tags_nonascii': True, 'lower_case': True,'expand_contractions': False, 'remove_punctuation': True, 'remove_escape_chars': True, 'remove_stopwords': True, 'remove_numbers': True, 'lemmatize': False, 'stemming': True, 'min_word_len': 3}

def clean_book(book_id,book):
	info_print("cleaner",str(book_id)+": Cleaning!")
	txt = preprocess_nlp.async_call_preprocess([book],clean_options)[0]
	return txt.replace(' . ',' ')

def clean_write_book(module,book_id,book):
	return write_book(module,False,book_id,clean_book(book_id,book))
	
