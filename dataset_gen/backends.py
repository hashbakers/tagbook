from helpers import *
import os
import cleaner
import json
import re
import requests

class OpenLibrary:
	status = {}
	def __init__(self):
		pass
	def get_general_tags(self):
		info_print("OpenLibrary","Retrieving tags...")
		text = requests.get('https://openlibrary.org/subjects').text
		data = re.findall('''<a href="(?:(/subjects/.+?)|/search\?q=subject%3A.+?&amp;subject_facet=.+?&amp;subject_facet=(.+?))">(.+?)</a>''',text)
		tags = []
		for elem in data:
			url = elem[0]
			if url == '':
				url = "/subjects/"+elem[1].lower()
			url = "https://openlibrary.org"+url+".json"
			tags.append({"url": url,"name": elem[2]})
		info_print("OpenLibrary","Got "+str(len(tags))+" tags!")
		return tags
	def get_books_from_tag(self,tag,n):
		info_print("OpenLibrary","Retrieving books from tag "+tag['name'])
		books_json = []
		for i in range(0,3):
			try:
				books_json = requests.get(tag['url']+"?details=true&limit="+("50" if n == None else str(n))).json()
				break
			except json.decoder.JSONDecodeError:
				if i == 2:
					info_print("OpenLibrary","Can't continue crawling tag "+tag['name'])
					return
				else:
					info_print("OpenLibrary","Error decoding JSON, retrying ("+str(i)+"/3)")
		books = []
		try:
			self.status[tag['name']] += 50 if n == None else n
		except KeyError:
			self.status[tag['name']] = 50 if n == None else n
		for i in range(0,len(books_json['works'])):
			try:
				if books_json['works'][i]['availability']['isbn'] != None:
					author = [a['name'] for a in books_json['works'][i]['authors']]
					books.append({'id':books_json['works'][i]['availability']['openlibrary_edition'],
								"title":books_json['works'][i]['title'],
								"general_tags":[tag['name']],
								"author":author,
								"isbn":[books_json['works'][i]['availability']['isbn']],
								"olid":books_json['works'][i]['availability']['openlibrary_edition']})
			except KeyError:
				continue
		return books 	
	def get_tags_for_book(self,book):
		#requires book to have OLID
		if book['olid'] == '':
			return
		info_print("OpenLibrary",book['olid']+": Retrieving tags...")
		try:
			data = requests.get('https://openlibrary.org/books/'+book['olid']+".json").json()
			data = requests.get('https://openlibrary.org'+data['works'][0]['key']+".json").json()
			if 'additional_tags' in book:
				book['additional_tags'] += data['subjects']
			else:
				book['additional_tags'] = data['subjects']
		except KeyError:
			info_print("OpenLibrary",book['olid']+": No additional tags found!")
		return
		
class ProjectGutenberg:
	def __init__(self):
		pass
	def get_general_tags(self):
		info_print("ProjectGutenberg","Retrieving tags...")
		text = requests.get('http://www.gutenberg.org/ebooks/bookshelf/').text

class LibGen:
	status = {}
	def __init__(self):
		pass
	def get_general_tags(self):
		info_print("LibGen","Retrieving tags...")
		text = requests.get('http://libgen.rs/').text
		#for now only the general categories
		data = re.findall('''<li><a href="../search.php\?req=(topicid\d+?)\&open=0\&column=topic" class="drop">(.+?)</a>''',text)
		tags = []
		for elem in data:
			if elem[1] == "Jurisprudence":
				continue
			tags.append({"url":"http://libgen.rs/search.php?req="+elem[0],"name":elem[1]})
		info_print("LibGen","Got "+str(len(tags))+" tags!")
		return tags
	def get_books_from_tag(self,tag,n):
		info_print("LibGen","Retrieving books from tag "+tag['name'])
		books = []
		self.status[tag['name']] = 1
		while len(books) < n:
			text = requests.get(tag['url']+"&page="+str(self.status[tag['name']])).text
			data = re.findall('''<tr valign=top .+?><td>(.+?)</td>''',text)
			if data == []:
				break
			debug_print("LibGen","Crawling page "+str(self.status[tag['name']])+"...")
			for elem in data:
				if len(books) >= n:
					break
				book_json = []
				for i in range(0,3):
					try:
						book_json = requests.get("http://libgen.rs/json.php?ids="+elem+"&fields=title,author,md5,identifier,openlibraryid,extension,language").json()[0]
						break
					except json.decoder.JSONDecodeError:
						if i == 2:
							info_print("LibGen","Can't continue crawling tag "+tag['name'])
							return
						else:
							info_print("LibGen","Error decoding JSON, retrying ("+str(i)+"/3)")
				if 'English' not in book_json['language']  or TextExtractor.check_book_supported({'format': book_json['extension']}) == False:
					continue
				books.append({'id':elem,
							'title':book_json['title'],
							'author':book_json['author'],
							'general_tags':[tag['name']],
							'md5':book_json['md5'],
							'isbn':book_json['identifier'].split(','),
							'olid':book_json['openlibraryid'],
							'format':book_json['extension'],
							'lgid':elem})
			debug_print("LibGen","Page "+str(self.status[tag['name']])+", have "+str(len(books))+" usable books")
			self.status[tag['name']] += 1
		return books	
	def check_avail(self,isbnn):
		ids = []
		for isbn in isbnn:
			info_print("LibGen",str(isbn)+": Checking availability...")
			text = requests.get('http://libgen.rs/search.php?req='+str(isbn)).text
			data = re.findall('''<tr valign=top .+?><td>(.+?)</td>''',text)
			info_print("LibGen",(str(isbn)+": not found!" if data == [] else str(isbn)+": Found "+str(len(data))+" versions!"))
			ids += data
		return (None if ids == [] else data)
	def download_book(self,book):
		#TODO: get links by OLID or ISBN?
		debug_print("LibGen",book['lgid']+": Crawling links...")
		text = requests.get('http://library.lol/main/'+str(book['md5'])).text
		data = re.findall('''<li><a href="(.+?)">(.+?)</a>''',text)
		if data == []:
			info_print("LibGen",book['lgid']+": No links found!")
			return False
		else:
			debug_print("LibGen",book['lgid']+": "+str(len(data))+" links found!")
		info_print("LibGen",book['lgid']+": Downloading using link 1...")
		book_content = requests.get(data[0][0]).content #use only first link for now
		try:
			os.makedirs("temp/libgen/",0o755)
		except FileExistsError:
			pass
		path = "temp/libgen/"+str(book['id'])+"."+book['format'] 
		with open(path,'wb') as f:
			f.write(book_content)
		book['raw_path'] = path
		return True
	def process_book(self,book,cleanup):
		if TextExtractor.check_book_supported(book) == False:
			return
		try:
			if book['raw_path'] == "":
				return
		except KeyError:
			return
		txt = TextExtractor.extract_text_from_book(book)
		if txt == None:
			return False
		if cleanup:
			txt = cleaner.clean_book(book['id'],txt)
			book['path'] = write_book("libgen",False,book['id'],txt)
		else:
			book['path'] = write_book("libgen",True,book['id'],txt)
		return True
