from helpers import *
import backends
import sys

def print_help():
	print("gen_db.py [mode] [num of entries]")
	print("modes:")
	print("[1] - full libgen extraction")
	print("[2] - full libgen extraction + cleanup")
	print("[3] - full libgen extraction + openlibrary tags")
	print("[4] - full libgen extraction + openlibrary tags + cleanup")

if len(sys.argv) < 3:
	print_help()
	exit()

books = []
n = int(sys.argv[2])
info_print("general","Starting for "+str(n)+" elements")
#get books
if int(sys.argv[1]) < 5 and int(sys.argv[1]) > 0:
	cleanup = True if sys.argv[1] == '2' or sys.argv[1] == '4' else False
	libgen = backends.LibGen()
	tags = libgen.get_general_tags()
	n_per_tag = int(n/len(tags))
	for tag in tags:
		local_books = libgen.get_books_from_tag(tag,n_per_tag)
		if local_books == None:
			info_print("general","Got nothing (reached the limit?), continuing with the tags...")
			continue
		info_print("general","Got "+str(len(local_books))+" books, merging!")
		merge_books(books,local_books)
	info_print("general","Finished crawling, let the downloads begin!")
	for book in books:
		k = libgen.download_book(book)
		if k == False:
			books.remove(book)
		k = libgen.process_book(book,cleanup)
		if k == False:
			books.remove(book)
if sys.argv[1] == '3' or sys.argv[1] == '4':
	ol = backends.OpenLibrary()
	info_print("general","Retrieving additional tags for books...")
	for book in books:
		if 'olid' in book:
			ol.get_tags_for_book(book)
#write books
f = open("db.csv", "w")
CSVWriter.write_header(f)
for book in books:
	CSVWriter.write_book(f,book)
f.close()
info_print("general","Done!")
