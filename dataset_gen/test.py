import backends
import helpers
import sys

def print_help():
	print("python test.py [cmd]")
	print("commands:")
	print("test_avail <tag provider> <content provider> - check availability ratio for given extractor")
	print("tag providers: openlib, libgen")
	print("content providers: libgen")

def test_avail(n,ol,lib):
	counter = {}
	counter_total = {}
	tags = ol.get_general_tags()
	for tag in tags:
		l = ol.get_books_from_tag(tag,n)
		counter[tag['name']] = 0
		counter_total[tag['name']] = len(tags)
		for el in l:
			print(el['title'],el['author'])
			if lib.check_avail(el['isbn']) != None:
				counter[tag['name']] += 1
	for keyname in counter:
		print(keyname,"-",counter[keyname],"available of",counter_total[keyname],"total")

def test_random_processing(n,tag,cont):
	counter = {}
	counter_total = {}
	tags = tag.get_general_tags()
	for tagx in tags:
		l = tag.get_books_from_tag(tagx,n)
		counter[tagx['name']] = 0
		counter_total[tagx['name']] = len(tags)
		for el in l:
			if helpers.TextExtractor.check_book_supported(el):
				cont.download_book(el)
				cont.process_book(el)
	return
	

if len(sys.argv) < 4:
	print_help()
	sys.exit()

tag_provider, content_provider = [], []
if sys.argv[2] == "libgen":
	tag_provider = backends.LibGen()
elif sys.argv[2] == "openlib":
	tag_provider = backends.OpenLibrary()
if sys.argv[3] == "libgen":
	content_provider = backends.LibGen()

if sys.argv[1] == "test_avail":
	test_avail(25,tag_provider,content_provider)
if sys.argv[1] == "test_random_processing":
	test_random_processing(5,backends.LibGen(),content_provider)
